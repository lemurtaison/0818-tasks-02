#include <stdio.h>
#include <iostream> 
#include <fstream> 
#include <stdlib.h> 
#include <conio.h> 
#include <iomanip> 
#include <ctime>
using namespace std;
int MaxCountOfDoubleArray(int **arr,int N1,int N2){
	int count1 = 0;
	int count2 = 0;
	int NumberOfColumn = 0;
	for (int i = 0; i < N2; i++) {
		for (int j = 0; j < N1; j++) {
			count1 += arr[j][i];
		}
		if (count1 > count2) {
			count2 = count1;
			NumberOfColumn = i;
		}
		count1 = 0;
	}
	return NumberOfColumn;

} 
int main(){ 
	int** arr;
	int N, M;
	cout << "write a size of massive NxM:" << endl;
	cin >> N >> M;
	arr = new int*[N];
	for (int i = 0; i < N; i++) {
		arr[i] = new int[M];
	}
	for (int i = 0; i < N; i++) {
		for (int f = 0; f < M; f++) {
			cout << "i=" << i+1 << " j=" << f+1 << endl;
			cin >> arr[i][f];
		}
	}
	cout << MaxCountOfDoubleArray(arr, N, M)+1;
	return 0;
}